# Générateur de Menus du Bar - Édition 2018 !

__Prérequis__
* Python >= 3.6
* Pillow
* Path.py
* (arrow pour le fichier d'exemple)

Le fichier main.py sert d'exemple quasi-fonctionnel

__Quickstart__

On commence par creer un objet Semaine :
```python
from semaine import *
semaine = Semaine()
```

Et on va pouvoir lui définir des jours :
```python
semaine["lundi"] = Vide()
```
Pour une journée avec rien d'affiché par exemple (pas besoin de le préciser à la limite puisque le constructeur `Semaine` suppose par défault que la semaine contient 5 jours vides)

On peut également définir des services avec 1 ou 2 plats (si on en met plus de 2 pour l'instant ça s'affiche mal)
```python
semaine["mardi"] = Service("Maïté")
semaine["mercredi"] = Service("Italien", "Chèvre")
```
Les noms des plats correspondent aux noms des fichiers dans `/assets/plats` sans l'extension

Un jour peut également être une journée "spéciale" prédéfinie
```python
semaine["jeudi"] = Spéciale("Repas Inté")
```
De la même manière Les noms des journées spéciales correspondent aux noms des fichiers dans `/assets/extras` sans l’extension

Enfin il est possible de définir n'importe quelle image pour une journée
```python
semaine["vendredi"] = Image("/le/chemin/vers/ton/image.jpg")
```
L'image sera alors redimensionnée pour prendre exactement la taille de la carte pour cette journée

Pour effectuer un rendu il suffit de faire
```python
semaine.rendu("/le/chemin/que/tu/veux.jpg")
```