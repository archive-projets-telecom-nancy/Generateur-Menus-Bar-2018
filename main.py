from semaine import *
import arrow

# On crée un objet semaine
semaine = Semaine()

# Pour une journée avec rien d'affiché
semaine["lundi"] = Vide()

# Pour un service avec un seul plat
semaine["mardi"] = Service("Maïté")

# Pour un service avec deux plats
semaine["mercredi"] = Service("Italien", "Chèvre")

# Exemple de journée spéciale (voir /assets/extras)
semaine["jeudi"] = Spéciale("Repas Inté")

# Pour afficher n'importe quelle image pour ce jour
semaine["vendredi"] = Image("/home/symeon/Images/CsLygN8UsAAtIHg.jpg")

chemin = f"Rendus/Menu {arrow.now().format('YYYY-MM-DD')}.jpg"

semaine.rendu(chemin)
