import PIL.Image
from path import Path
from plats import *

class RenderWarning(Warning):
    pass

class Journée():
    
    _taille = (343,714)

    def rendu(self):
        pass

class Vide(Journée):
    
    def rendu(self):
        
        return PIL.Image.new("RGBA",self._taille)

class Service(Journée):
    
    _x_position = 29
    _y_position = 131
    _y_increment = 295

    def __init__(self,*plats):

        self.plats = list(map(Plat.get_by_name,plats))

    def rendu(self):

        carte_du_service = PIL.Image.new("RGBA",self._taille)

        for i,plat in enumerate(self.plats):

            icone = PIL.Image.open(plat.img_path).convert("RGBA")
            if icone.size != (273,273):
                raise RenderWarning(f"La taille d'icone pour le {plat.name} est différente de 273x273, elle sera redimensionnée")
                icone = icone.resize((273,273),PIL.Image.BILINEAR)

            carte_du_service.alpha_composite(icone,(self._x_position,self._y_position+i*self._y_increment))

        return carte_du_service

class Image(Journée):
    
    def __init__(self, path_image):

        self.path_image = path_image

    def rendu(self):

        return PIL.Image.open(self.path_image).convert("RGBA").resize(self._taille,PIL.Image.BILINEAR)

class Spéciale(Image):
    
    _dossier_speciaux = Path("assets/extras")
    
    def __init__(self, nom):
        
        for chemin in Spéciale._dossier_speciaux.walkfiles():
            if chemin.stem == nom:
                self.path_image = chemin
                return
                
        raise Exception(f"Pas de journée spéciale trouvée pour le nom : {nom}")
