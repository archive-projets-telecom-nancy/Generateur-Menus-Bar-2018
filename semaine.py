import PIL.Image
from path import Path
from journée import *

class SemaineError(Exception):
    pass

class Semaine(dict):
    
    _dossier_fonds = Path("assets/fonds")
    
    _x_position = 61
    _y_position = 255
    _x_increment = 364
    
    _jours = ["lundi","mardi","mercredi","jeudi","vendredi"]
    
    def __init__(self, journées=[Vide()]*5, fond=None):
        
        self.journées = journées
        self.fond = fond or self.get_bg_by_name("Bois - Légumes Rouge")
    
    def __setitem__(self, key, item):
        
        _key = key.strip().lower()
        
        if _key not in self._jours:
            raise SemaineError(f"{key} n'est pas un jour de la semaine !")
        else:
            self.journées[self._jours.index(_key)] = item
    
    def rendu(self,filepath,conversion="RGB",format="jpeg"):

        #Si le dossier n'existe pas on le crée
        Path(filepath).parent.makedirs_p()
        
        fond_semaine = PIL.Image.open(self.fond).convert("RGBA")
        
        for i,jour in enumerate(self.journées):
            
            carte_jour = jour.rendu()
            pos_jour = (self._x_position+i*self._x_increment,self._y_position)
            fond_semaine.alpha_composite(carte_jour,pos_jour)
        
        fond_semaine.convert(conversion).save(filepath,format)
    
    @classmethod
    def get_bg_by_name(cls,nom):
        for path in cls._dossier_fonds.walkfiles():
            if path.stem == nom:
                return path
        