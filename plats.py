from path import Path

class Plat():
    
    _dossier_plats = Path("assets/plats")
    
    def __init__(self,name,img_path):
        self.name = name
        self.img_path = img_path
    
    @classmethod
    def defaults(cls):
        for chemin_image_plat in cls._dossier_plats.walkfiles():
            yield Plat(chemin_image_plat.stem, chemin_image_plat)
    
    @classmethod
    def get_by_name(cls,nom):
        for plat in Plat.defaults():
            if plat.name == nom:
                return plat
        raise Exception(f"Pas de plat trouvé pour le nom : {nom}")

dico_plats = {x.name:x for x in Plat.defaults()}

    